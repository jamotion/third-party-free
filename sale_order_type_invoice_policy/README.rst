.. |company| replace:: ADHOC SA

.. |company_logo| image:: https://raw.githubusercontent.com/ingadhoc/maintainer-tools/master/resources/adhoc-logo.png
   :alt: ADHOC SA
   :target: https://www.adhoc.com.ar

.. |icon| image:: https://raw.githubusercontent.com/ingadhoc/maintainer-tools/master/resources/adhoc-icon.png

.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

================================
Sale Order Type Invoicing Policy
================================

Add to sale order type invoice policy field with possible values:

* Defined by Product: default odoo behaviour, invoice qty will be computed considering product configuration.
* Ordered quantities: overwrites product behaviour so that all lines are considered as "ordered quantities" no matter product configuration, once order is confirmed all products should be invoiced.
* Before Delivery: similar than ordered quantities but it requires a pay invoice for every line before order confirmation.
* This module is also integrated with sale_usability_return_invoicing so that returns for "Before Delivery" or "Ordered quantities" can be returned.

Installation
============

To install this module, you need to:

#. Only need to install the module

Configuration
=============

To configure this module, you need to:

#. Nothing to configure

Usage
=====

To use this module, you need to:


Credits
=======

Images
------

* |company| |icon|

Contributors
------------

* Jamotion <info@jamotion.ch>