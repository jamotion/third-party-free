import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_invoice_paid(self):
        res = super(AccountInvoice, self).action_invoice_paid()
        orders = self.mapped('invoice_line_ids').mapped('sale_line_ids').mapped('order_id')
        orders.filtered(lambda f: f.type_invoice_policy == 'prepaid').action_confirm()
        return res
